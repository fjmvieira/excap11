public class ValorInvalidoException extends RuntimeException {


	public ValorInvalidoException() {
		super("Valor invalido");
	}

	public ValorInvalidoException(double valor) {
                super("Valor invalido : "+ valor);
        }
}